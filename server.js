const express = require('express');
const hbs = require('hbs');
const fs = require('fs');

const port = process.env.PORT || 3000;
const app = express();

hbs.registerPartials(__dirname+'/views/partials');
hbs.registerHelper('getCurrentYear',()=>{
   return new Date().getFullYear();
});

app.set('view engine','hbs');


//Maintenance Middleware
// app.use((req,res,next)=>{
//     res.render('maintenance.hbs');
// });

app.use(express.static(__dirname + '/public'));
app.use((req,res,next)=>{
   var log = new Date().toString();
   fs.appendFileSync('server.log',`Method:${req.method} time:${log}\n`);
   next();
});

app.get('/',(req,res)=>{
   res.render('home.hbs',{
     pageTitle:'Home Page',
     welcomeMessage:'Welcome to my website'
   });
});

app.get('/bad',(req,res)=>{
   res.send({
       errorMessage:'The page is not Available'
   });
});

app.get('/blogs',(req,res)=>{
   res.render('blogs.hbs',{
       pageTitle:'Blogs Page',
       welcomeMessage:'My Blogs'
   });
});

app.get('/about',(req,res)=>{
//    res.send('<div>About Page</div>')
    res.render('about.hbs',{
        pageTitle:'About Page',
    });
});


app.listen(port,()=>{
   console.log(`Listening on port ${port}`); 
})